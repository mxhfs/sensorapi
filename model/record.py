from db.database import db
import datetime
from sqlalchemy.dialects.mysql import DATETIME
from model.sensor import SensorDataType


class SensorRecord(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(DATETIME(fsp=6), nullable=False, default=datetime.datetime.now)
    value = db.Column(db.String(255), nullable=False)

    sensor = db.relationship("Sensor", back_populates="sensor_records")
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'))

    user = db.relationship("User")
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def serialize_value(self):
        if self.sensor.dataType == SensorDataType.DOUBLE:
            return float(self.value);
        else:
            return self.value

    def serialize(self):
        return {
            'timestamp': self.timestamp.isoformat(),
            'value': self.serialize_value()
        }
