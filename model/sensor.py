from db.database import db
from enum import Enum, auto


class SensorDataType(Enum):
    DOUBLE = auto()
    STRING = auto()

    @staticmethod
    def list():
        return SensorDataType.DOUBLE.name, SensorDataType.STRING.name


class Sensor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    dataType = db.Column(db.Enum(SensorDataType), nullable=False, default=SensorDataType.DOUBLE)
    sensor_records = db.relationship('SensorRecord', cascade='delete', back_populates="sensor")

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'dataType': self.dataType.name
        }
