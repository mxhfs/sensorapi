# Show sensor by id

Shows all available Sensors in Database.

**URL** : `/sensor/sensor_id`

**Authentication Required** : :x:

**Method** : `GET`

## Success Responses

**Code** : `200 OK`

**Content** : The serialized version of the requested sensor

### Example

**Request**: `GET /sensor/1`

**Response**:
```json
  {
    "dataType": "DOUBLE",
    "id": 1,
    "name": "Testsensor"
  }
```

## Error Responses

**Code** : `404 NOT FOUND` if sensor does not exist



# Edit sensor

Change name of an existing sensor in database.

**URL** : `/sensor/sensor_id`

**Authentication Required** : :heavy_check_mark:

**Method** : `PUT`

## Success Responses

**Code** : `201 CREATED`

**Content** : a list of all Senors stored in the database

### Example

**Request**: `PUT /sensor/1`

**Request-Body**:
```json
{
    "name": "New Sensor"
}
```

**Response**:
```json
  {
    "dataType": "DOUBLE",
    "id": 1,
    "name": "New Sensor"
  }
```
## Error Responses

**Code** : `404 NOT FOUND` if sensor does not exist



# Delete sensor

Deletes an existing sensor.

**URL** : `/sensor/sensor_id`

**Authentication Required** : :heavy_check_mark:

**Method** : `DELETE`

## Success Responses

**Code** : `204 NO CONTENT`

**Content** : `EMPTY`

### Example

**Request**: `DELETE /sensor/1`


**Response**:
```json
```

## Error Responses

**Code** : `404 NOT FOUND` if sensor does not exist
