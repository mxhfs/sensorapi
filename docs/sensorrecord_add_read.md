# Show all records

Shows all available Records for aSensors in Database.

**URL** : `/sensor/sensor_id/record`

**Authentication Required** : :x:

**Method** : `GET`

## Success Responses

**Code** : `200 OK`

**Content** : The serialized version of the requested sensor records

### Example

**Request**: `GET /sensor/1/record`

**Response**:
```json
[
  {
    "timestamp": 1626728228478,
    "value": "1.3"
  },
  {
    "timestamp": 1626728244518,
    "value": "1.5"
  },
  {
    "timestamp": 1626728249534,
    "value": "1.2"
  },
  {
    "timestamp": 1626728272816,
    "value": "1.2"
  }
]
```

## Error Responses

**Code** : `404 NOT FOUND` if sensor does not exist



# Add sensor record

Adds a record to an existing sensor in database.

**URL** : `/sensor/sensor_id/record`

**Authentication Required** : :heavy_check_mark:

**Method** : `POST`

## Success Responses

**Code** : `201 CREATED`

**Content** : The serialized version of the saved sensor record

### Example

**Request**: `POST /sensor/1/record`

**Request-Body**:
```json
{
    "value": "1.3"    
}
```

**Response**:
```json
{
  "timestamp": 1626728378047,
  "value": "1.3"
}
```
## Error Responses

**Code** : `404 NOT FOUND` if sensor does not exist

