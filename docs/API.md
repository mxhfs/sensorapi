# API-Documentation

To call Api-Functions that need Authentication the `Authorization` Header needs to be set. To retrieve a valid token, you need a `username` and a `password`. The token can be retrieved by the [`auth/login`](auth.md#login-user) call. 

**Example HTTP-Header**: `Authorization: Bearer mVzaCI6ZmFsc2Us.ImlhdCI6MTYwNjc4OCwianR.pIjoiMWUzNGIyMGYtZThkNy00Yz`

If you **don't already have a user**, you can create one using the [`auth/login`](auth.md#add-user) call.

## All available API-Functions

For all Api-Calls the HTTP-Header `Content-Type` must be set to `application/json`

* [Signup and Login](auth.md) : `auth`
* [List available Sensors](sensor_list.md) : `/` or `sensor/list`
* [Show/Edit/Delete Sensors](sensor_show_edit_delete.md) : `/sensor/<sensor_id>`
* [Add Sensor](sensor_add.md) : `sensor`
* [Add/Read Sensor Records](sensorrecord_add_read.md) : `/sensor/<sensor_id>/record`
