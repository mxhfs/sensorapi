# Show all available Sensors

Shows all available Sensors in Database.

**URL** : `/` or `/sensor/list`

**Authentication Required** : :x:

**Method** : `GET`

## Success Responses

**Code** : `200 OK`

**Content** : a list of all Senors stored in the database

```json
[
  {
    "dataType": "DOUBLE",
    "id": 1,
    "name": "Testsensor"
  },
  {
    "dataType": "DOUBLE",
    "id": 2,
    "name": "Testsensor2"
  }
]
```
