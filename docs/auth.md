# Add User

Adds a new user to the database.

**URL** : `/auth/signup`

**Method** : `POST`

**Authentication Required** : :heavy_check_mark:

## Success Responses

**Code** : `201 CREATED`

**Content** : The name of the new user

### Example

**Request**: `POST /auth/signup`

**Request-Body**:
```json
{
    "name": "username",
    "password": "secret"
}
```

**Response**:
```json
{
  "id": 1,
  "name": "username"
}
```

## Error Responses

**Code** : `400 BAD REQUEST` if the request body is not well-formed

**Response**
```json
{
  "message": {
    "name": "name is required parameter!",
    "password": "password is required parameter!"
  }
}
```


# Login User

Logs in a user to retrieve an access token.

**URL** : `/auth/login`

**Method** : `POST`

**Authentication Required** : :x:

## Success Responses

**Code** : `200 OK`

**Content** : The name of the new user

### Example

**Request**: `POST /auth/login`

**Request-Body**:
```json
{
    "name": "username",
    "password": "secret"
}
```

**Response**:
```json
{
  "token": "mVzaCI6ZmFsc2Us.ImlhdCI6MTYwNjc4OCwianR.pIjoiMWUzNGIyMGYtZThkNy00Yz"
}
```

## Error Responses

**Code** : `400 BAD REQUEST` if the request body is not well-formed

**Response**
```json
{
  "message": {
    "name": "name is required parameter!",
    "password": "password is required parameter!"
  }
}
```