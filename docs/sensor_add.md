# Add sensor

Adds a new sensor to the database.

**URL** : `/sensor`

**Method** : `POST`

**Authentication Required** : :heavy_check_mark:

## Success Responses

**Code** : `201 CREATED`

**Content** : The serialization of the new sensor

### Example

**Request**: `POST /sensor`

**Request-Body**:
```json
{
    "name": "New Sensor"
}
```

**Response**:
```json
  {
    "id": 1,
    "dataType": "DOUBLE",
    "name": "New Sensor"
  }
```

## Error Responses

**Code** : `400 BAD REQUEST` if the request body is not well-formed

**Response**
```json
{
  "message": {
    "name": "name is required parameter!"
  }
}
```