from service.record_service import RecordAdd
from service.sensor_service import SensorList, SensorApi, SensorAdd
from service.auth_service import SignupApi, LoginApi
from flask_restful import Api
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager


def set_resources(_app):
    api = Api(_app)
    bcrypt = Bcrypt(_app)
    jwt = JWTManager(_app)

    api.add_resource(SensorList, '/sensor/list', '/')
    api.add_resource(RecordAdd, '/sensor/<sensor_id>/record')
    api.add_resource(SensorApi, '/sensor/<sensor_id>')
    api.add_resource(SensorAdd, '/sensor')
    api.add_resource(SignupApi, '/auth/signup')
    api.add_resource(LoginApi, '/auth/login')
