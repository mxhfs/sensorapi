from flask import request, jsonify
from flask_restful import Resource
from model.record import SensorRecord
from model.sensor import Sensor, SensorDataType
from model.user import User
from db.database import db
from service.parsers import record_parser
from http import HTTPStatus
from flask_jwt_extended import jwt_required, get_jwt_identity


class RecordAdd(Resource):

    @staticmethod
    def get(sensor_id: int):
        sensor = Sensor.query.filter_by(id=sensor_id).first_or_404(
            description='Sensor with id={} is not available'.format(sensor_id))
        return jsonify([SensorRecord.serialize(record) for record in sensor.sensor_records])

    @staticmethod
    @jwt_required()
    def post(sensor_id: int):
        request.get_json(force=True)
        args = record_parser.parse_args()
        user_id = get_jwt_identity()

        sensor = Sensor.query.filter_by(id=sensor_id).first_or_404(
            description='Sensor with id={} is not available'.format(sensor_id))
        if sensor.dataType == SensorDataType.DOUBLE:
            try:
                float(args['value'])
            except ValueError:
                return 'Value [{}] can not be parsed to double'.format(args['value']), HTTPStatus.BAD_REQUEST
        sensor_record = SensorRecord(sensor=sensor, value=str(args['value']))

        user = User.query.filter_by(id=user_id)\
            .first_or_404(description='User with id={} is not available'.format(user_id))
        sensor_record.user = user
        db.session.add(sensor_record)
        db.session.commit()
        return SensorRecord.serialize(sensor_record), HTTPStatus.CREATED
