from flask import jsonify, request
from flask_restful import Resource
from model.sensor import Sensor
from db.database import db
from service.parsers import sensor_parser
from http import HTTPStatus
from flask_jwt_extended import jwt_required


class SensorList(Resource):

    @staticmethod
    def get():
        records = Sensor.query.all()
        return jsonify([Sensor.serialize(record) for record in records])


class SensorApi(Resource):

    @staticmethod
    def get(sensor_id):
        return jsonify(Sensor.serialize(
            Sensor.query.filter_by(id=sensor_id).first_or_404(description='Sensor with id={} is not available'
                                                              .format(sensor_id))))

    @staticmethod
    @jwt_required()
    def put(sensor_id):
        request.get_json(force=True)
        args = sensor_parser.parse_args()
        sensor_record = Sensor.query.filter_by(id=sensor_id).first_or_404(
            description='Sensor with id={} is not available'.format(sensor_id))
        print(args)
        sensor_record.name = args['name']
        db.session.add(sensor_record)
        db.session.commit()
        return Sensor.serialize(sensor_record), HTTPStatus.CREATED

    @staticmethod
    @jwt_required()
    def delete(sensor_id):
        sensor_record = Sensor.query.filter_by(id=sensor_id).first_or_404(
            description='Sensor with id={} is not available'.format(sensor_id))
        db.session.delete(sensor_record)
        db.session.commit()
        return 'Sensor with id={} deleted'.format(sensor_id), HTTPStatus.NO_CONTENT


class SensorAdd(Resource):

    @staticmethod
    @jwt_required()
    def post():
        request.get_json(force=True)
        args = sensor_parser.parse_args()
        sensor_record = Sensor(name=args['name'], dataType=args['dataType'])
        db.session.add(sensor_record)
        db.session.commit()
        return Sensor.serialize(sensor_record), HTTPStatus.CREATED
