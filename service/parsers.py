from flask_restful import reqparse
from model.sensor import SensorDataType

sensor_parser = reqparse.RequestParser(bundle_errors=True)
sensor_parser.add_argument('name',  required=True, location=('json',), help="name is required parameter!")
sensor_parser.add_argument('dataType', choices=(SensorDataType.list()), location=('json',),
                           help="DataType is not supported! Supported types are {supported_types}"
                           .format(supported_types=SensorDataType.list()))


record_parser = reqparse.RequestParser(bundle_errors=True)
record_parser.add_argument('value', required=True, location=('json',), help="value is required parameter!")


user_parser = reqparse.RequestParser(bundle_errors=True)
user_parser.add_argument('name',  required=True, location=('json',), help="name is required parameter!")
user_parser.add_argument('password',  required=True, location=('json',), help="password is required parameter!")
