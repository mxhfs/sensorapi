# Sensor API

The sensor API can be used to store and request timestamped records of sensor measures

# Build and run

To build the API, the following command can be used:

```bash
docker build -t lelefan/sensorapi .
```

To run the software the container can be started with the following command:

```bash
docker run -d lelefan/sensorapi:latest
```

Available Environment variables

|Name| Value (default)|  Description|
|----|-------|------------|
|`MYSQL_CONNECTION_STRING`| `/`|If this Variable is set, the application will use a mysql database. Otherwise a SQLite DB will be used. Syntax: `DB_USER:DB_PASSWORD@DB_HOST:DB_PORT/DB_NAME`|
|`DEFAULT_USERNAME`|`admin`|  Username for a user that can call functions that need authorization|
|`DEFAULT_PASSWORD`|`admin`|Password for a user that can call functions that need authorization|
|`JWT_KEY` |`insecureDefaultKey`|Encrpytion key to store passwords|
 

# Documentation

The API-Documentation can be found [here](docs/API.md)
