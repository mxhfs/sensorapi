import unittest
import json
from app import app
from db.database import db
from model.sensor import Sensor
from model.user import User
from http import HTTPStatus
from unittest import TestCase

APPLICATION_JSON = 'application/json'


class TestSensorService(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.db = db
        self.authToken = self.get_auth_token()

    def test_get_sensor_list(self):
        url = '/sensor/list'

        clear_sensor_db()

        response = self.app.get(url)
        self.assertEqual(HTTPStatus.OK, response.status_code)
        verify_response_sensor_list(response.get_data())
        response = self.app.get('/')
        self.assertEqual(HTTPStatus.OK, response.status_code)
        verify_response_sensor_list(response.get_data())
        response = self.app.get(url + '/')
        self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)

        # Create a sensor
        create_sensor('test')
        response = self.app.get(url)
        self.assertEqual(HTTPStatus.OK, response.status_code)
        verify_response_sensor_list(response.get_data())
        response = self.app.get('/')
        self.assertEqual(HTTPStatus.OK, response.status_code)
        verify_response_sensor_list(response.get_data())
        response = self.app.get(url + '/')
        self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)

    def test_get_sensor(self):
        url = '/sensor/{}'

        clear_sensor_db()

        sensor_id = create_sensor('test')
        response = self.app.get(url.format(sensor_id))
        self.assertEqual(HTTPStatus.OK, response.status_code)
        response = self.app.get(url.format(sensor_id + 1))
        self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)

    def test_post_sensor(self):
        url = '/sensor'

        clear_sensor_db()

        response = self.app.post(url, content_type=APPLICATION_JSON,
                                 headers=self.authToken, data='{"name":"testSensor"}')
        self.assertEqual(HTTPStatus.CREATED, response.status_code)
        response = self.app.post(url, content_type=APPLICATION_JSON,
                                 headers=self.authToken)
        self.assertEqual(HTTPStatus.BAD_REQUEST, response.status_code)

    def test_auth_signup(self):
        url = '/auth/signup'

        clear_user_db()

        response = self.app.post(url, content_type=APPLICATION_JSON,
                                 headers=self.authToken, data='{"name":"admin", "password":"admin"}')
        self.assertEqual(HTTPStatus.BAD_REQUEST, response.status_code)
        response = self.app.post(url, content_type=APPLICATION_JSON,
                                 headers=self.authToken, data='{"name":"admin12", "password":"admin"}')
        self.assertEqual(HTTPStatus.CREATED, response.status_code)

    def test_auth_login(self):
        url = '/auth/login'

        response = self.app.post(url, data='{"name":"admin", "password":"admin"}', content_type=APPLICATION_JSON)
        self.assertEqual(HTTPStatus.OK, response.status_code)
        response = self.app.post(url, data='{"name":"admin", "password":""}', content_type=APPLICATION_JSON)
        self.assertEqual(HTTPStatus.UNAUTHORIZED, response.status_code)

    def test_get_sensor_record(self):
        url = '/sensor/{}/record'

        clear_sensor_db()
        sensor_id = create_sensor('test')

        response = self.app.get(url.format(sensor_id), content_type=APPLICATION_JSON)
        self.assertEqual(HTTPStatus.OK, response.status_code)
        response = self.app.get(url.format(sensor_id + 1), content_type=APPLICATION_JSON)
        self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)

    def test_post_sensor_record(self):
        url = '/sensor/{}/record'

        clear_sensor_db()
        sensor_id = create_sensor('test')

        response = self.app.post(url.format(sensor_id), headers=self.authToken,
                                 data='{"value": "1.1"}', content_type=APPLICATION_JSON)
        self.assertEqual(HTTPStatus.CREATED, response.status_code)
        response = self.app.post(url.format(sensor_id), headers=self.authToken,
                                 data='{"value": "ab"}', content_type=APPLICATION_JSON)
        self.assertEqual(HTTPStatus.BAD_REQUEST, response.status_code)
        response = self.app.post(url.format(sensor_id + 1), headers=self.authToken,
                                 data='{"value": "ab"}', content_type=APPLICATION_JSON)
        self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)

    def get_auth_token(self):
        url = 'auth/login'

        response = self.app.post(url, data='{"name":"admin", "password":"admin"}',
                                 content_type=APPLICATION_JSON)
        data = json.loads(response.get_data())
        return {"Authorization": "Bearer " + data['token']}


def create_sensor(name):
    with app.app_context():
        sensor = Sensor(name=name)
        db.session.add(sensor)
        db.session.commit()
        return sensor.id


def clear_sensor_db():
    with app.app_context():
        sensors = Sensor.query.all()
        for sensor in sensors:
            db.session.delete(sensor)
        db.session.commit()


def clear_user_db():
    with app.app_context():
        users = User.query.filter(User.name != 'admin').all()
        for user in users:
            db.session.delete(user)
        db.session.commit()


def verify_response_sensor_list(data):
    result = json.loads(data)
    if len(result) == 0:
        TestCase.assertEqual(TestCase(), json.loads(data), [])
    else:
        for sensor in result:
            TestCase.assertTrue(TestCase(), expr='dataType' in sensor)
            TestCase.assertTrue(TestCase(), expr='id' in sensor)
            TestCase.assertTrue(TestCase(), expr='name' in sensor)


if __name__ == "__main__":
    unittest.main()
